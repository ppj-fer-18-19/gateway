package hr.fer.ppp.parkmefer.gateway.config;

import hr.fer.ppp.parkmefer.gateway.GatewayApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import reactor.core.publisher.Mono;

@Configuration
public class LoggingConfiguration {

    private static final Logger GLOBAL_LOGGER = LoggerFactory.getLogger(GatewayApplication.class);
    private static final String REQUEST_LOG_FORMAT = "Request {method: %s, host: %s, port: %d, path: %s, query_params: %s}";
    private static final String RESPONSE_LOG_FORMAT = "Response {status: %s}";

    @Bean
    @Order(0)
    public GlobalFilter loggingFilter() {
        return (exchange, chain) -> {
            GLOBAL_LOGGER.info(String.format(REQUEST_LOG_FORMAT,
                    exchange.getRequest().getMethod(),
                    exchange.getRequest().getURI().getHost(),
                    exchange.getRequest().getURI().getPort(),
                    exchange.getRequest().getURI().getPath(),
                    exchange.getRequest().getQueryParams()));
            return chain.filter(exchange).then(Mono.fromRunnable(() -> {
                GLOBAL_LOGGER.info(String.format(RESPONSE_LOG_FORMAT,exchange.getResponse().getStatusCode()));
            }));
        };
    }
}
